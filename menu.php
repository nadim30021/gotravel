<?php
// checked if user is logged in or not
session_start();
if (isset($_SESSION["username"]) && isset($_SESSION["isLoggedIn"])) {
    if ($_SESSION["isLoggedIn"] != true) {
        header("location: login.php");
    }

} else {
    header("location: login.php");
}
?>


<!-- menu prepare -->


<head>

    <style>
        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            border: 1px solid #e7e7e7;
            background-color: #f3f3f3;
        }

        li {
            float: left;
        }

        li a {
            display: block;
            color: #666;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
        }

        li a:hover:not(.active) {
            background-color: #ddd;
        }

        li a.active {
            color: white;
            background-color: #4CAF50;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/like_button_style.css"/>
    <script src="js/like_button.js" type="text/javascript"></script>
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
</head>

<ul>
    <li><a class="active" href="index.php">Home</a></li>
    <li><a href="my_story.php">My Story</a></li>
    <li><a href="profile.php">Profile Update</a></li>
    <li style="float: right"><a href="logout.php">Logout</a></li>
</ul>